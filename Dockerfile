FROM ubuntu:17.10
LABEL maintainer="Arnaud Fonce" description="Java 8 Zulu Image"
ENV ZULU_ARCHIVE zulu8.23.0.3-jdk8.0.144-linux_x64.tar.gz
RUN apt-get update && apt-get install -y --no-install-recommends && apt-get install wget unzip -y && rm -rf /var/lib/apt/lists/* \
       && wget -O /tmp/zulu.tar.gz https://cdn.azul.com/zulu/bin/zulu8.28.0.1-jdk8.0.163-linux_x64.tar.gz \
       && mkdir -p /usr/lib/jvm \
       && tar -C /usr/lib/jvm/ -xzvf /tmp/zulu.tar.gz \
       && rm -f /tmp/zulu.tar.gz \
       && ln -s /usr/lib/jvm/zulu8.28.0.1-jdk8.0.163-linux_x64 /usr/lib/jvm/java-8
ENV JAVA_HOME /usr/lib/jvm/java-8
ENV PATH "$PATH":/${JAVA_HOME}/bin

